# Interaktivní evoluce hudby pomocí rekurentních NN - semestrální práce na MI-MVI

Program generuje množinu krátkých, nekolikavteřinových hudebních sekvencí 
v MIDI formátu. Tyto sekvence jsou ve webovém GUI přehrány uživateli.

Uživatel jednotlivé sekvence ohodnotí. Hodnocení pak slouží jako vstup rekurentní 
neuronové sítě, kteráse snaží vytvořit co nejzajímavější a uchu posluchače 
nejpříjemnější hudbu.

## Zprovoznění aplikace

Program se skládá ze dvou částí -- backendové (Python) a frontendové 
(Javascript + Node.js).

### Závislosti

Pro zprovoznění aplikace je třeba mít na počítači [Python 3](https://www.python.org), 
[pip](https://pip.pypa.io) a [npm](https://www.npmjs.com).

Pak je třeba nainstalovat závislosti pro backend...

`pip3 install -r backend/requirements.txt`

...a frontend...

`cd frontend`

`npm install`

### Spuštění

V adresáři `frontend` spusťte:

`npm run server`

Pokud se vše podaří, spustí se webový server na adrese `localhost:3000`


(Občas se po prvním spuštění nenačítají zvuky. Webovou stránku je tedy ještě třeba 
ještě jednou obnovit.)

Aplikace je v terminálu trochu ukecanější, takže pokud něco vypisuje, 
není to pravděpodobně příznakem chyby.

### Dodatek: Použití backendové části

Backend napsaný v Pythonu se dá teoreticky používat sám o sobě. 
Se světem komunikuje přes stdin a stdout, a to ve formátu JSON.

Po spuštění programu (soubor `backend/evolve_music/__init__.py`) se na výstup 
vypíše JSON pole obsahující MIDI soubory zakódované jako Base64 stringy.

Po zadání vstupu (JSON pole integerů reprezentující hodnocení uživatele) 
program na výstup vypíše další hudbu k hodnocení.
